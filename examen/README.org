#+SETUPFILE:/home/masliah/dev/org-html-themes/setup/theme-bigblow.setup
#+OPTIONS: ^:nil
#+AUTHOR: Ian Masliah
#+EMAIL: ian.masliah@inria.fr

** Composition du Jury

- Ian Masliah
- Oguz Kaya
- Mathieu Faverge

** Présentation d'un article (le 25/01/2018 à 9h30)

Par groupe de 2 ou 3 (un seul groupe) 

- Francais ou Anglais au choix
- 7 minutes par personne 
- 10 minutes de question à la fin

** Comment organiser une présentation un article 

- Introduction des thèmes du papier, l'intêret de celui-ci
- Discussion de l'état de l'art sur le domaine si il y a
- Présentation des apports du papier au domaine
- Présentation et discussion des performances 
- Conclusion générale sur ce qui a été présenté, votre avis sur la question

** Choix des articles 

- Data Movement Aware Computation Partitioning : Khattabi riffi Adnane/ Magnaldo Paul 
- Selective GPU Caches to Eliminate CPU–GPU HW Cache Coherence : Alexis Dufrenne / Maxence Ronzié / Ahcene Mahtout
- Novel HPC Techniques to Batch Execution of Many Variable Size BLAS Computations on GPUs : Paul Beziau / Xavier Dussieux
- Code modernization strategies to 3-D Stencil-based applications on Intel Xeon Phi: KNC and KNL : Pierre Roux / Raphaël Tapia
- Capability Models for Manycore Memory Systems: A Case-Study with Xeon Phi KNL : Le Corguillé Arthur / Saux Thomas