#include <iostream>
#include "timer.hpp"
#include "nbody-struct-soa.hpp"
#include "init.hpp"

int main( int argc, char * argv[] )
{
  std::size_t size  = 1000
            , steps = 10;

  if( argc != 3 )
  {
    std::cerr << "E: Incorrect number of arguments, using " << size << " particles and " << steps << " steps." << std::endl;
  }
  else {
    size  = std::stoll( argv[ 1 ], 0, 10 );
    steps = std::stoll( argv[ 2 ], 0, 10 );
  }

  particles ps( size );

  init_particules_soa(ps);

  auto duration = timer_nbody(nbody_soa_step,ps,steps);  

  std::cout << "soa " << "generic " << duration  << std::endl;

  if( size < 64 )
  {
    for(int i = 0 ; i < p.x.size() ; ++i) 
    {
      std::cout << "( " << p.x[i] << ", " << p.y[i] << ", " << p.z[i] << " )"<< std::endl;
    }
  }

  return 0;
}
