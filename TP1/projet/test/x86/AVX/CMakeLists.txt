set( SOURCES
    nbody-struct-aos-avx.cpp
    nbody-struct-soa-avx.cpp
)
foreach( SOURCE ${SOURCES} )
    get_filename_component( BIN ${SOURCE} NAME_WE )
    list( APPEND BINS ${BIN})
    add_executable( ${BIN} ${SOURCE} )  
    set_target_properties(${BIN} PROPERTIES
          RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

endforeach( SOURCE )
             
