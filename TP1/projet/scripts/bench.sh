#!/bin/bash

echo "You can provide the minimum and maximum sizes the script will test on." 

if [ -z "$1" ]
then
    minsize=1000
else
    minsize=$1
fi

if [ -z "$2" ]
then
    maxsize=10000
else
    maxsize=$2
fi

echo "Tests will run from size $minsize to size $maxsize" 


DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
FILES=$DIR/../bin/*
BINDIR=$DIR/../bin/
results=$DIR/../results

mkdir -p $results

# Get hardware counter names
$DIR/../bin/nbody-struct-aos 100 1 > tmp.txt
papi_counters=$(grep ":" tmp.txt | cut -d ':' -f 1 | sed 's/ *$//' | tr '\n' ',') 
header=$(echo "$papi_counters size,struct,type,time  " | tr -d '[:space:]')

echo "hardware counters : $papi_counters"

for f in $FILES
do
    if echo "$f" | grep -q "nbody-struct"; then
        cur_file=$(echo "${f##$BINDIR}" )
        echo $header > $results/$cur_file.csv
        echo "Processing $cur_file"
        for i in {$minsize..$maxsize..1000} 
        do
            $BINDIR/$cur_file $i 10 > tmp.txt
            res="$i $(tail -n 1 tmp.txt)"
            res=${res// /,}
            line=$(grep ":" tmp.txt | cut -d ':' -f 2 | sed 's/ *$//' | tr '\n' ',' | sed -e "s/$/$res\\n/" ) 
            echo "${line// /}" >> $results/$cur_file.csv
            
        done
    fi
done

rm $DIR/../tmp.txt
