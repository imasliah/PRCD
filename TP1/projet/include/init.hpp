#pragma once

template<class Particule>
void init_particules_aos(Particule & ps)
{
  for( std::size_t i = 0 ; i < ps.size() ; ++i )
  {
    ps[ i ].x = ( 0.5f + i - ps.size() / 2 ) * 10.0f;
    ps[ i ].y = 0.0f;
    ps[ i ].z = 0.0f;
    ps[ i ].m = 1.0f;
  }
}


template<class Particule>
void init_particules_soa(Particule & ps)
{
  for( std::size_t i = 0 ; i < ps.x.size() ; ++i )
  {
   ps.x[ i ] = ( 0.5f + i - ps.x.size() / 2 ) * 10.0f;
   ps.y[ i ] = 0.0f;
   ps.z[ i ] = 0.0f;
   ps.m[ i ] = 1.0f;
  }
}
