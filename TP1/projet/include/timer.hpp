#pragma once 

#include <chrono>
#include <vector>
#include <algorithm>
#include <functional>

#define NB_RUNS 10

#ifdef use_papi
#include <papi.h>
// The number of hardware counters to look at and their value -- Beware only a small number
// can be read at the same time. Run the papi_avail executable to see available counters
#define NUM_EVENTS 3
static int Events[NUM_EVENTS] = {PAPI_L1_DCM,PAPI_L1_ICM,PAPI_TOT_INS};

// The list of counters to display -- Should be changed adequately
template<typename T>
void display_events( T & e ) 
{
std::cout << "-----------------------------Events-------------------------" << std::endl;
std::cout << "Level 1 data cache misses                   : " << e[0] << std::endl;
std::cout << "Level 1 instruction cache misses            : " << e[1] << std::endl;
std::cout << "Instructions completed                      : " << e[2] << std::endl;
std::cout << "------------------------------------------------------------" << std::endl;
}
#endif

template<class Func, class Part>
auto timer_nbody(const Func& nbody_aos_step, Part& ps, const int steps) -> double
{

#ifdef use_papi
  std::vector<long long> values(NUM_EVENTS);
  std::vector<long long> result_values(NUM_EVENTS,0);

  auto retval = PAPI_library_init( PAPI_VER_CURRENT );
  if(retval != PAPI_VER_CURRENT) std::cout << "not current papi version" << std::endl;

  retval = PAPI_start_counters( Events, NUM_EVENTS);

  if (retval != PAPI_OK)
  {
  std::cout << " start counters error : " << retval << std::endl;
  }
#endif

  using type = double;
          
  std::vector<type> runs(NB_RUNS);
   
  for( int run = 0 ; run < runs.size() ; ++run)
  {
#ifdef use_papi    
    auto retval = PAPI_read_counters(values.data() ,NUM_EVENTS);
#endif 

    auto start = std::chrono::system_clock::now();
    
    for( std::size_t step = 0 ; step < steps ; ++step )
    {
      nbody_aos_step( ps );
    }

    auto stop = std::chrono::system_clock::now();

#ifdef use_papi
    retval = PAPI_read_counters(values.data() , NUM_EVENTS);
    if (retval != PAPI_OK) {std::cout << " read counters error : " << retval << std::endl;exit(1);}

    for(size_t i = 0 ; i < NUM_EVENTS ; ++i)
    {
      result_values[i] += values[i] ;
    }
#endif 

    auto duration = stop - start;

    runs[run] = std::chrono::duration_cast< std::chrono::milliseconds >( duration ).count();
  } 

  std::sort(runs.begin(), runs.end(), std::greater<type>());

#ifdef use_papi
  for(size_t i =0 ; i < NUM_EVENTS ; ++i)
  {
    result_values[i] = result_values[i] / NB_RUNS ;
  }

  display_events(result_values);
#endif

  return runs[NB_RUNS/2];
}
