#pragma once

#include <vector>
#include <cmath>

#ifdef use_iaca
#include "iacaMarks.h"
#else
#define IACA_START
#define IACA_END
#endif

//TODO : write soa version
struct particles
{
  std::vector< float > x, y, z, m , vx, vy, vz;

  particles( std::size_t size ): x( size )
                               , y( size )
                               , z( size )
                               , m( size )
                               , vx( size )
                               , vy( size )
                               , vz( size )
                               {}
};

struct accel
{
  float x, y, z;
};

//TODO : write avx version
void nbody_soa_avx_step( particles & ps )
{
  IACA_START;

  for( std::size_t i = 0 ; i < ps.size() ; ++i )
  {
    accel a{ 0.0f, 0.0f, 0.0f };

    for( std::size_t j = i + 1 ; j < ps.size() ; ++j )
    {
      auto dx = ps[ j ].x - ps[ i ].x;
      auto dy = ps[ j ].y - ps[ i ].y;
      auto dz = ps[ j ].z - ps[ i ].z;

      auto inorm = 0.1f / sqrtf( dx * dx + dy * dy + dz * dz + 0.00125f );
      auto fi = ps[ j ].m * inorm * inorm * inorm;
      auto fj = ps[ i ].m * inorm * inorm * inorm;

      a.x += dx * fi;
      a.y += dy * fi;
      a.z += dz * fi;

      ps[ j ].vx -= dx * fj;
      ps[ j ].vy -= dy * fj;
      ps[ j ].vz -= dz * fj;
    }

    ps[ i ].vx += a.x;
    ps[ i ].vy += a.y;
    ps[ i ].vz += a.z;

    ps[ i ].x += ps[ i ].vx;
    ps[ i ].y += ps[ i ].vy;
    ps[ i ].z += ps[ i ].vz;
  }
  IACA_END;

}
