cmake_minimum_required( VERSION 2.8 )

project(adc_tp1)

list(APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/find)

option(BUILD_WITH_IACA
        "Enable Intel static code analyser" OFF)

option(BUILD_WITH_PAPI
        "Enable PaPi hardware counter" OFF)

set( CMAKE_CXX_COMPILER "g++" )
set( CMAKE_CXX_FLAGS "-std=c++11 -O2 -march=native -save-temps " )

if(BUILD_WITH_IACA)
 add_definitions(-Duse_iaca)
 include_directories( iaca )
 configure_file(${CMAKE_SOURCE_DIR}/iaca/iaca bin/iaca COPYONLY)
endif()

if(BUILD_WITH_PAPI)
	find_package(PAPI)
	if (PAPI_FOUND)
        add_definitions(-Duse_papi)
        include_directories( ${PAPI_INCLUDE_DIRS} )
        link_directories( ${PAPI_LIBRARY_DIRS} )
        link_libraries( ${PAPI_LIBRARIES} )
        set(LIBRARIES ${LIBRARIES} ${PAPI_LIBRARIES})
        include_directories(${PAPI_INCLUDE_DIRS})
	else()
        message(FATAL_ERROR "PAPI is required but was not found. Please provide a PAPI library in your environment with PAPI_DIR")
    endif()
endif()

include_directories( include )
add_subdirectory( test )

configure_file(${CMAKE_SOURCE_DIR}/mlc/mlc bin/mlc COPYONLY)
configure_file(${CMAKE_SOURCE_DIR}/mlc/mlc_avx512 bin/mlc_avx512 COPYONLY)
configure_file(${CMAKE_SOURCE_DIR}/scripts/bench.sh scripts/bench.sh COPYONLY)
configure_file(${CMAKE_SOURCE_DIR}/scripts/graph.R scripts/graph.R COPYONLY)


