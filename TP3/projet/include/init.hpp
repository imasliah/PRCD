#pragma once

template<class Particule>
void init_particules_aos(Particule & ps)
{
  for( std::size_t i = 0 ; i < ps.size() ; ++i )
  {
    ps[ i ].x = ( 0.5f + i - ps.size() / 2 ) * 10.0f;
    ps[ i ].y = 0.0f;
    ps[ i ].z = 0.0f;
  }
}


template<class Particule>
void init_particules_soa(Particule & ps)
{
  for( std::size_t i = 0 ; i < ps.x.size() ; ++i )
  {
   ps.x[ i ] = ( 0.5f + i - ps.x.size() / 2 ) * 10.0f;
   ps.y[ i ] = 0.0f;
   ps.z[ i ] = 0.0f;
  }
}


#ifdef __NVCC__

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/copy.h>

template<class Particule>
void init_particules_aos_cuda(Particule & ps)
{
  thrust::host_vector<typename Particule::value_type> h_part(ps.size());

  for( std::size_t i = 0 ; i < ps.size() ; ++i )
  {
    h_part[ i ].x = ( 0.5f + i - ps.size() / 2 ) * 10.0f;
    h_part[ i ].y = 0.0f;
    h_part[ i ].z = 0.0f;
  }

  thrust::copy(h_part.begin(), h_part.end(), ps.begin());
}


template<class Particule_init>
void init_particules_soa_cuda(Particule_init & ps_i, int size)
{  

  thrust::host_vector<float> h_x(size);
  thrust::host_vector<float> h_y(size);
  thrust::host_vector<float> h_z(size);

  for( std::size_t i = 0 ; i < size ; ++i )
  {
   h_x[ i ] = ( 0.5f + i - size/ 2 ) * 10.0f;
   h_y[ i ] = 0.0f;
   h_z[ i ] = 0.0f;
  }

  ps_i.x = h_x;
  ps_i.y = h_y;
  ps_i.z = h_z;
}

#endif 