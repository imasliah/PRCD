#pragma once

#include <vector>
#include <cmath>

#define BLOCK_SIZE 32

struct particles
{
  float *x, *y, *z , *vx, *vy, *vz;
};

struct particles_init
{
  thrust::device_vector< float > x, y, z , vx, vy, vz;

  particles_init( std::size_t size ): x( size )
                               , y( size )
                               , z( size )
                               , vx( size )
                               , vy( size )
                               , vz( size )
                               {}
};

struct accel
{
  float x,y,z;
};

__global__ void nbody_step_soa_cuda_call( float * psx, float * psy, float * psz, float * pvx
                                        , float * pvy, float * pvz, int size)
{
  int i = blockDim.x * blockIdx.x + threadIdx.x;
 
  if (i < size) 
  {
    accel a{ 0.0f, 0.0f, 0.0f };

    for( std::size_t j = 0; j < size ; ++j )
    {
      auto dx = psx[ j ] - psx[ i ];
      auto dy = psy[ j ] - psy[ i ];
      auto dz = psz[ j ] - psz[ i ];
  
      auto inorm = 0.1f / sqrtf( dx * dx + dy * dy + dz * dz + 0.00125f );
      auto fi = inorm * inorm * inorm;
  
      a.x += dx * fi;
      a.y += dy * fi;
      a.z += dz * fi;
    }

    pvx[ i ] += a.x;
    pvy[ i ] += a.y;
    pvz[ i ] += a.z;  
      
    psx[ i ] += pvx[ i ];
    psy[ i ] += pvy[ i ];
    psz[ i ] += pvz[ i ];
  }
}

void nbody_soa_step_cuda( particles & ps, int size )
{

  int nBlocks = (size + BLOCK_SIZE - 1) / BLOCK_SIZE;

  nbody_step_soa_cuda_call<<<nBlocks,BLOCK_SIZE>>>(ps.x, ps.y, ps.z, ps.vx, ps.vy, ps.vz, size);

  cudaDeviceSynchronize();
}
