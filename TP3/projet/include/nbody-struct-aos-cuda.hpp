#pragma once

#include <vector>
#include <cmath>

#define BLOCK_SIZE 32

struct particle
{
  float x, y, z, m , vx, vy, vz;
};

struct accel
{
  float x,y,z;
};

using particles = thrust::device_vector< particle >;

__global__ void nbody_step_aos_cuda_call(particle *ps, int size)
{
  int i = blockDim.x * blockIdx.x + threadIdx.x;

  if (i < size) 
  {
    accel a{ 0.0f, 0.0f, 0.0f };

    for( std::size_t j = 0; j < size ; ++j )
    {
      auto dx = ps[ j ].x - ps[ i ].x;
      auto dy = ps[ j ].y - ps[ i ].y;
      auto dz = ps[ j ].z - ps[ i ].z;
  
      auto inorm = 0.1f / sqrtf( dx * dx + dy * dy + dz * dz + 0.00125f );
  
      auto fi = inorm * inorm * inorm;
  
      a.x += dx * fi;
      a.y += dy * fi;
      a.z += dz * fi;
    }

    ps[ i ].vx += a.x;
    ps[ i ].vy += a.y;
    ps[ i ].vz += a.z;  
      
    ps[ i ].x += ps[ i ].vx;
    ps[ i ].y += ps[ i ].vy;
    ps[ i ].z += ps[ i ].vz;
  }
}

void nbody_aos_step_cuda( particle * ps, int size )
{
  int nBlocks = (size + BLOCK_SIZE - 1) / BLOCK_SIZE;

  nbody_step_aos_cuda_call<<<nBlocks,BLOCK_SIZE>>>(ps,size);
  cudaDeviceSynchronize();
}
