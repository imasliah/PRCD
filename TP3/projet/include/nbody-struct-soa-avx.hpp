#pragma once

#include <vector>
#include <cmath>

#ifdef use_iaca
#include "iacaMarks.h"
#else
#define IACA_START
#define IACA_END
#endif

//TODO : write soa version
struct particles
{
  std::vector< float > x, y, z, m , vx, vy, vz;

  particles( std::size_t size ): x( size )
                               , y( size )
                               , z( size )
                               , m( size )
                               , vx( size )
                               , vy( size )
                               , vz( size )
                               {}
};

struct accel
{
  float x, y, z;
};

//TODO : write avx version
void nbody_soa_avx_step( particles & ps )
{
  IACA_START;

  for( std::size_t i = 0; i < ps.x.size() ; ++i )
  {
    accel a{ 0.0f, 0.0f, 0.0f };

    for( std::size_t j = 0; j < ps.x.size() ; ++j )
    {
      auto dx = ps.x[ j ] - ps.x[ i ];
      auto dy = ps.y[ j ] - ps.y[ i ];
      auto dz = ps.z[ j ] - ps.z[ i ];

      auto inorm = 0.1f / sqrtf( dx * dx + dy * dy + dz * dz + 0.00125f );
      auto fi =  inorm * inorm * inorm;

      a.x += dx * fi;
      a.y += dy * fi;
      a.z += dz * fi;
    }

    ps.vx[ i ] += a.x;
    ps.vy[ i ] += a.y;
    ps.vz[ i ] += a.z;

    ps.x[ i ] += ps.vx[ i ];
    ps.y[ i ] += ps.vy[ i ];
    ps.z[ i ] += ps.vz[ i ];
  }
  IACA_END;
}
