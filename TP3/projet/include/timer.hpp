#pragma once 

#include <chrono>
#include <vector>
#include <algorithm>
#include <functional>

#define NB_RUNS 10

#ifdef use_papi
#include <papi.h>
#define PAPI_ERROR(n,v) (fprintf(stderr, "%s failed with code %d\n", (n), (v)))
// The number of hardware counters to look at and their value -- Beware only a small number
// can be read at the same time. Run the papi_avail executable to see available counters
#define NUM_EVENTS 3

static int Events[NUM_EVENTS] = {PAPI_L1_DCM,PAPI_L1_ICM,PAPI_TOT_INS};

static const std::vector<std::string> CUDA_Events = {"cuda:::metric:inst_executed:device=0"
                                                    ,"cuda:::event:l1_shared_load_transactions:device=0"
                                                    // ,"cuda:::metric:flop_count_sp:device=0"
                                                    ,"cuda:::event:uncached_global_load_transaction:device=0"
                                                    };


// The list of counters to display -- Should be changed adequately
template<typename T>
void display_events( T & e ) 
{
std::cout << "-----------------------------Events-------------------------" << std::endl;
std::cout << "Level 1 data cache misses                   : " << e[0] << std::endl;
std::cout << "Level 1 instruction cache misses            : " << e[1] << std::endl;
std::cout << "Instructions completed                      : " << e[2] << std::endl;
std::cout << "------------------------------------------------------------" << std::endl;
}

template<typename T>
void display_events_cuda( T & e ) 
{
std::cout << "-----------------------------Events-------------------------" << std::endl;
std::cout << "Instructions completed                      : " << e[0] << std::endl;
std::cout << "Level 1 shared load transactions            : " << e[1] << std::endl;
std::cout << "uncached global load transactions           : " << e[2] << std::endl;
std::cout << "------------------------------------------------------------" << std::endl;
}

#endif

template<class Func, class Part>
auto timer_nbody(const Func& nbody_step, Part& ps, const int steps) -> double
{

#ifdef use_papi
  std::vector<long long> values(NUM_EVENTS);
  std::vector<long long> result_values(NUM_EVENTS,0);

  auto retval = PAPI_library_init( PAPI_VER_CURRENT );
  if(retval != PAPI_VER_CURRENT) std::cout << "not current papi version" << std::endl;

  retval = PAPI_start_counters( Events, NUM_EVENTS);

  if (retval != PAPI_OK)
  {
  std::cout << " start counters error : " << retval << std::endl;
  }
#endif

  using type = double;
          
  std::vector<type> runs(NB_RUNS);
   
  for( int run = 0 ; run < runs.size() ; ++run)
  {
#ifdef use_papi    
    auto retval = PAPI_read_counters(values.data() ,NUM_EVENTS);
#endif 

    auto start = std::chrono::system_clock::now();
    
    for( std::size_t step = 0 ; step < steps ; ++step )
    {
      nbody_step( ps );
    }

    auto stop = std::chrono::system_clock::now();

#ifdef use_papi
    retval = PAPI_read_counters(values.data() , NUM_EVENTS);
    if (retval != PAPI_OK) {std::cout << " read counters error : " << retval << std::endl;exit(1);}

    for(size_t i = 0 ; i < NUM_EVENTS ; ++i)
    {
      result_values[i] += values[i] ;
    }
#endif 

    auto duration = stop - start;

    runs[run] = std::chrono::duration_cast< std::chrono::milliseconds >( duration ).count();
  } 

  std::sort(runs.begin(), runs.end(), std::greater<type>());

#ifdef use_papi
  for(size_t i =0 ; i < NUM_EVENTS ; ++i)
  {
    result_values[i] = result_values[i] / NB_RUNS ;
  }

  display_events(result_values);
#endif

  return runs[NB_RUNS/2];
}


# ifdef __NVCC__

#include <thrust/functional.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>

template<class Func, class Part>
auto timer_nbody_cuda(const Func& nbody_step, Part & d_ptr, const int steps, int size) -> double
{

#ifdef use_papi

  int EventSetCUDA = PAPI_NULL;
  int EventCode;
  std::vector<long long> values(CUDA_Events.size());
  std::vector<long long> result_values(CUDA_Events.size(),0);

  auto retval = PAPI_library_init( PAPI_VER_CURRENT );
  if(retval != PAPI_VER_CURRENT) std::cout << "not current papi version" << std::endl;

  PAPI_create_eventset(&EventSetCUDA);

  for(int i = 0; i < CUDA_Events.size(); ++i)
  {
    retval = PAPI_event_name_to_code(CUDA_Events[i].c_str(), &EventCode);

    if (retval != PAPI_OK)
      PAPI_ERROR("PAPI_event_name_to_code", retval);

    retval = PAPI_add_event(EventSetCUDA, EventCode);

    if (retval != PAPI_OK)
      PAPI_ERROR("PAPI_add_events", retval);
  }

  retval = PAPI_start(EventSetCUDA);
  if (retval != PAPI_OK)
      PAPI_ERROR("PAPI_start", retval);
#endif

  using type = double;
          
  std::vector<type> runs(NB_RUNS);

  for( int run = 0 ; run < runs.size() ; ++run)
  {
#ifdef use_papi    
    auto retval = PAPI_accum(EventSetCUDA, values.data());
#endif 

    auto start = std::chrono::system_clock::now();
    
    for( std::size_t step = 0 ; step < steps ; ++step )
    {
      nbody_step(d_ptr,size) ;
    }

    auto stop = std::chrono::system_clock::now();

#ifdef use_papi
    retval = PAPI_accum(EventSetCUDA, values.data());
    if (retval != PAPI_OK) {std::cout << " read counters error : " << retval << std::endl;exit(1);}

    for(size_t i = 0 ; i < NUM_EVENTS ; ++i)
    {
      result_values[i] += values[i] ;
    }
#endif 

    auto duration = stop - start;

    runs[run] = std::chrono::duration_cast< std::chrono::milliseconds >( duration ).count();
  } 

  std::sort(runs.begin(), runs.end(), std::greater<type>());

#ifdef use_papi
  for(size_t i =0 ; i < CUDA_Events.size() ; ++i)
  {
    result_values[i] = result_values[i] / NB_RUNS ;
  }

  display_events_cuda(result_values);
#endif

  return runs[NB_RUNS/2];
}

#endif