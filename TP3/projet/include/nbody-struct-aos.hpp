#pragma once

#include <vector>
#include <cmath>

#ifdef use_iaca
#include "iacaMarks.h"
#else
#define IACA_START
#define IACA_END
#endif

struct particle
{
  float x, y, z, vx, vy, vz;
};

struct accel
{
  float x, y, z;
};

using particles = std::vector < particle >;

void nbody_aos_step( particles & ps )
{
  IACA_START;

  for( std::size_t i = 0; i < ps.size() ; ++i )
  {
    accel a{ 0.0f, 0.0f, 0.0f };

    for( std::size_t j = 0; j < ps.size() ; ++j )
    {
      auto dx = ps[ j ].x - ps[ i ].x;
      auto dy = ps[ j ].y - ps[ i ].y;
      auto dz = ps[ j ].z - ps[ i ].z;

      auto inorm = 0.1f / sqrtf( dx * dx + dy * dy + dz * dz + 0.00125f );
      auto fi = inorm * inorm * inorm;

      a.x += dx * fi;
      a.y += dy * fi;
      a.z += dz * fi;
    }

    ps[ i ].vx += a.x;
    ps[ i ].vy += a.y;
    ps[ i ].vz += a.z;

    ps[ i ].x += ps[ i ].vx;
    ps[ i ].y += ps[ i ].vy;
    ps[ i ].z += ps[ i ].vz;
  }
  IACA_END;

}
