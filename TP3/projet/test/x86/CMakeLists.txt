add_subdirectory( generic )
add_subdirectory( SSE )
add_subdirectory( AVX )
add_subdirectory( CUDA )
