#include <iostream>
#include "timer.hpp"
#include "nbody-struct-soa-cuda.hpp"
#include "init.hpp"

int main( int argc, char * argv[] )
{
  std::size_t size  = 1000
            , steps = 10;

  if( argc != 3 )
  {
    std::cerr << "E: Incorrect number of arguments, using " << size << " particles and " << steps << " steps." << std::endl;
  }
  else {
    size  = std::stoll( argv[ 1 ], 0, 10 );
    steps = std::stoll( argv[ 2 ], 0, 10 );
  }

  particles_init ps_i( size );
  particles ps;

  init_particules_soa_cuda(ps_i,size);

  ps.x = thrust::raw_pointer_cast(ps_i.x.data());
  ps.y = thrust::raw_pointer_cast(ps_i.y.data());
  ps.z = thrust::raw_pointer_cast(ps_i.z.data());
  ps.vx = thrust::raw_pointer_cast(ps_i.vx.data());
  ps.vy = thrust::raw_pointer_cast(ps_i.vy.data());
  ps.vz = thrust::raw_pointer_cast(ps_i.vz.data());

  auto duration = timer_nbody_cuda(nbody_soa_step_cuda,ps,steps,size);  

  std::cout << "soa " << "cuda " << duration  << std::endl;

  if( size < 64 )
  {
    thrust::host_vector<float> h_x = ps_i.x; 
    thrust::host_vector<float> h_y = ps_i.y; 
    thrust::host_vector<float> h_z = ps_i.z; 

    for(int i = 0 ; i < size ; ++i) 
    {
      std::cout << "( " << h_x[i] << ", " << h_y[i] << ", " << h_z[i] << " )"<< std::endl;
    }
  }

  return 0;
}
