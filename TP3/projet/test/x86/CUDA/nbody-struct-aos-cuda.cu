#include <iostream>
#include "timer.hpp"
#include "nbody-struct-aos-cuda.hpp"
#include "init.hpp"

int main( int argc, char * argv[] )
{
  std::size_t size  = 1000
            , steps = 10;

  if( argc != 3 )
  {
    std::cerr << "E: Incorrect number of arguments, using " << size << " particles and " << steps << " steps." << std::endl;
  }
  else {
    size  = std::stoll( argv[ 1 ], 0, 10 );
    steps = std::stoll( argv[ 2 ], 0, 10 );
  }

  particles ps( size );

  init_particules_aos_cuda(ps);

  auto * d_ptr = thrust::raw_pointer_cast(ps.data());

  auto duration = timer_nbody_cuda(nbody_aos_step_cuda,d_ptr,steps,size);  

  std::cout << "aos " << "cuda " << duration  << std::endl;

  if( size < 64 )
  {
    thrust::host_vector<particle> h_ptr = ps; 

    for( auto p: h_ptr )
    {
      std::cout << "( " << p.x << ", " << p.y << ", " << p.z << " )"<< std::endl;
    }
  }

  return 0;
}
