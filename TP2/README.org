* Introduction

La base de tout langage de programmation commence avec l’écriture d’un hello
world. Pour compiler le programme helloworld, entrez la ligne suivante :

#+begin_src
nvcc -o hello helloworld.cu
#+end_src

Pour exécuter, il suffit ensuite de lancer l’exécutable :
#+begin_src
./hello
#+end_src

Dans le cas présent, avec un GPU de type Kepler, vous devriez voir un ”HELLO
WORLD !” qui est affiché avec un caractère par ligne. Maintenant, si vous augmentez
le nombre de blocs dans le programme que va t-il se passer sachant que le GPU est une
plate-forme de calcul parallèle ? Comment pouvez-vous expliquer ce phénomène ?


* Addition de vecteurs


L’addition vectorielle est un bon exemple pour comprendre l’allocation mémoire et
la distribution des données sur un GPU en gardant un kernel assez basique. Après avoir
regarder les notions de cudaMemcpy, cudaMalloc, blocs et threads modifier
le code addition vectorielle.cu pour qu’il compile et fonctionne correctement. Utiliser la
ligne de compilation suivante :

#+begin_src
nvcc - o vector addition vectorielle.cu
#+end_src

Le résultat à obtenir est c [ 4194303 ] = 8388606.


* Stencil 1D

Dans le fichier 1Dstencil.cu vous trouverez un exemple de filtre permettant d’appréhender
la notion de mémoire partagée d’un GPU. Dans un premier temps, faites tourner le code
sans utiliser la mémoire partagée et utiliser une fonction de mesure de temps pour timer
le code.
Essayez ensuite d’utiliser la mémoire partagée et faites tourner votre code plusieurs
fois. Les résultats vous sembles t’ils correct ? Que devez-vous faire pour corriger cela ?
Les performances sont elles les même avec et sans la mémoire partagée ?
Utiliser la ligne de compilation suivante :

#+begin_src
nvcc - o stencil 1Dstencil.cu
#+end_src


* Inversion d'un vecteur

Écrivez un programme en CUDA qui prend un vecteur en entrée et renvoie son inverse (le premier élément en dernier et ainsi de suite).

Vous écrirez deux kernels effectuant cette opération, un qui n'utilise pas la shared memory et un l'utilisant.

Comparez les performances de vos 2 kernels et expliquez les.


* Produit de vecteurs

Écrire un produit vecteur-vecteur sur GPU et vérifier que le code fonctionne.
Essayer maintenant de comparer les performances sur différentes tailles de matrices
avec une version CPU que vous écrirez comprenant du openmp et de la vectorisation.
Que pouvez-vous en déduire de l’utilité d’un GPU dans le calcul scientifique ?

